
(function ($) {
  
Drupal.behaviors.nrelateCommonFrontend = {
  attach: function(context, settings) {
    jQuery.getScript(settings.nrelate.common_frontend, function(data, textStatus) {
      nRelate.domain = settings.nrelate.domain;
      
      if (typeof(settings.nrelate.related_url) != 'undefined') {
        var entity_decoded_nr_url = jQuery("<span/>").html(settings.nrelate.related_url).text();
        nRelate.getNrelatePosts(entity_decoded_nr_url);
      }
    });
  }
};
  
})(jQuery);
