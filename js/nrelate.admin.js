
(function ($) {

// A Drupal version of checkad(nr_domain,nr_admin_version,nr_key) from WP
// http://api.nrelate.com/common_wp/0.03.0/adcheck.php?domain=wordpress.localhost&nr_key=GVGXzvPVo8nfCbK08o3EIJ0p&getrequest=1&_=1317407520378
Drupal.behaviors.nrelateVerifyAd = {
  attach: function(context, settings) {
    $('#adverify').once('adverify', function() {
      // TODO: For security reasons, we should switch to AJAX calls or JSONP calls.
      jQuery.getScript("http://api.nrelate.com/common_dr/"+settings.nrelate.admin_version+"/adcheck.php?domain="+settings.nrelate.domain+"&key="+settings.nrelate.key+"&getrequest=1");
    });
  }
};

// A Drupal version of getnrcode(nr_domain,nr_admin_version,nr_key) from WP
// http://api.nrelate.com/common_wp/0.03.0/getnrcode.php?domain=wordpress.localhost&nr_key=GVGXzvPVo8nfCbK08o3EIJ0p&_=1317407520527
Drupal.behaviors.nrelateGetAdCode = {
  attach: function(context, settings) {
    $('#getnrcode').once('getnrcode', function() {
      // TODO: For security reasons, we should switch to AJAX calls or JSONP calls.
      jQuery.getScript("http://api.nrelate.com/common_dr/"+settings.nrelate.admin_version+"/getnrcode.php?domain="+settings.nrelate.domain+"&key="+settings.nrelate.key);
    });
  }
};

// A Drupal version of checkindex(nr_settingsurl,nr_domain,nr_admin_version) from WP
// http://api.nrelate.com/common_wp/0.03.0/indexcheck.php?domain=wordpress.localhost&getrequest=1&_=1317407520375
Drupal.behaviors.nrelateCheckIndex = {
  attach: function(context, settings) {
    $('#indexcheck').once('indexcheck', function() {
      // TODO: For security reasons, we should switch to AJAX calls or JSONP calls.
      jQuery.getScript("http://api.nrelate.com/common_dr/"+settings.nrelate.admin_version+"/indexcheck.php?domain="+settings.nrelate.domain+"&getrequest=1");
    });
  }
};

})(jQuery);
