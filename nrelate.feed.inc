<?php

function nrelate_node_feed($superkey = -1) {
  global $base_url, $language_content;
  
  if (isset($_GET['key']) && $_GET['key'] == variable_get('nrelate_feed_key', '')) {
    if (isset($_GET['debug']) && $_GET['debug']) {
      return nrelate_node_feed_debug($superkey);
    }
    
    $namespaces = array('xmlns:dc' => 'http://purl.org/dc/elements/1.1/');
    $channel = array();
  
    $types = variable_get('nrelate_feed_types', array_keys(node_type_get_names()));
    if (!empty($types)) {
      $query = db_select('node', 'n')
        ->fields('n', array('nid', 'created'))
        ->condition('n.status', 1)
        ->condition('n.type', $types, 'IN')
        ->orderBy('n.nid', 'DESC')
        ->range(0, variable_get('nrelate_feed_size', 5))
        ->addTag('node_access');
  
      if ($superkey >= 0) {
        $query->condition('n.nid', $superkey, '<');
      }
  
      $nids = $query->execute()->fetchCol();
    }
  
    $items = '';
    if ($nids) {
      // Load all nodes to be rendered.
      $nodes = node_load_multiple($nids);
      $last_nid = 0;
      foreach ($nodes as $node) {
        $item_text = '';
    
        $node->link = url("node/$node->nid", array('absolute' => TRUE));
        $node->rss_namespaces = array();
        $node->rss_elements = array(
          array(
            'key' => 'pubDate',
            'value' => gmdate('r', $node->created),
          ),
          array(
            'key' => 'guid',
            'value' => $node->nid . ' at ' . $base_url,
            'attributes' => array('isPermaLink' => 'false'),
          ),
        );
    
        // The node gets built and modules add to or modify $node->rss_elements
        // and $node->rss_namespaces.
        $build = node_view($node, 'nrelate_rss');
        unset($build['#theme']);
    
        if (!empty($node->rss_namespaces)) {
          $namespaces = array_merge($namespaces, $node->rss_namespaces);
        }
    
        // We render node contents and force links to be last.
        $build['links']['#weight'] = 1000;
        $item_text .= drupal_render($build);
        
        // Try and strip JS first before XSS check, else only SCRIPT tags get removed but code remains.
        $item_text = preg_replace('~<\s*\bscript\b[^>]*>(.*?)<\s*\/\s*script\s*>~is', ' ', $item_text);
        
        // We must strip html tags
        $item_text = filter_xss($item_text, array());
        
        // Remove extra whitespace
        $item_text = preg_replace('/\s+/is', ' ', $item_text);
        
        // Try to prepend a thumbnail image
        if ($thumbnail = _nrelate_get_node_thumbnail($node)) {
          $item_text = '<p>' . $thumbnail . '</p>' . $item_text;
        }
    
        $items .= format_rss_item($node->title, $node->link, $item_text, $node->rss_elements);
    
        $last_nid = $node->nid;
      }
  
      $channel['dc:relation'] = url('nrelate-rss.xml/'. $last_nid, array('absolute' => TRUE));
    }
  
    $channel_defaults = array(
      'version' => '2.0', 
      'title' => variable_get('site_name', 'Drupal'), 
      'link' => $base_url, 
      'description' => variable_get('feed_description', ''), 
      'language' => $language_content->language,
    );
    $channel_extras = array_diff_key($channel, $channel_defaults);
    $channel = array_merge($channel_defaults, $channel);
  
    $output = "<?xml version=\"1.0\" encoding=\"utf-8\"?>\n";
    $output .= "<rss version=\"" . $channel["version"] . "\" xml:base=\"" . $base_url . "\" " . drupal_attributes($namespaces) . ">\n";
    $output .= format_rss_channel($channel['title'], $channel['link'], $channel['description'], $items, $channel['language'], $channel_extras);
    $output .= "</rss>\n";
  
    drupal_add_http_header('Content-Type', 'application/rss+xml; charset=utf-8');
    print $output;
    return;
  }
  
  // If we hit this point return 404.
  drupal_not_found();
}

function nrelate_node_feed_debug($superkey = NULL) {
  $output = array();
  
  // TODO: output some debug info
  
  return $output;
}

function _nrelate_get_node_thumbnail($node) {
  $thumbnail = '';
  
  // Try custom image field first.
  if ($field = variable_get('nrelate_feed_thumbnail_field', '')) {
    // TODO: set thumbnail to url of image
    if (isset($node->{$field}[LANGUAGE_NONE][0]['uri'])) {
      $thumbnail = theme('image', array(
        'path' => file_create_url($node->{$field}[LANGUAGE_NONE][0]['uri']),
        'alt' => t('post thumbnail'),
        'attributes' => array('class' => array('nrelate-image', 'custom-field-image')),
      ));
    }
  }
  
  // Else, try to find an image from the media module and use the first one
  // Code modeled after media.filter.inc
  if (empty($thumbnail) && isset($node->body[LANGUAGE_NONE][0]['value']) && module_exists('media')) {
    // eg. [[{"type":"media","view_mode":"media_large","fid":"101","attributes":{"alt":"","class":"media-image","typeof":"foaf:Image"}}]]
    preg_match('#\[\[(\{.+})]]#isU', $node->body[LANGUAGE_NONE][0]['value'], $tags);
    if (isset($tags[1])) {
      $tag_info = drupal_json_decode($tags[1]);
      
      if (isset($tag_info['fid'])) {
        if ($file = file_load($tag_info['fid'])) {
          $thumbnail = theme('image', array(
            'path' => file_create_url($file->uri),
            'alt' => t('post thumbnail'),
            'attributes' => array('class' => array('nrelate-image', 'auto-content-image')),
          ));
        }
      }
    }
  }
  
  // Else, try to find a raw image tag in the body and use the first one.
  if (empty($thumbnail) && isset($node->body[LANGUAGE_NONE][0]['value'])) {
    preg_match('#<img[^>]+src=[\"\']{1}(http:\/\/.*\.(gif|png|jpg|jpeg|tif|tiff|bmp){1})[\"\']{1}[^>]+\/>#i', $node->body[LANGUAGE_NONE][0]['value'], $images);
    if (isset($images[1])) {
      $thumbnail = theme('image', array(
        'path' => $images[1],
        'alt' => t('post thumbnail'),
        'attributes' => array('class' => array('nrelate-image', 'auto-content-image')),
      ));
    }
  }
  
  return $thumbnail;
}
