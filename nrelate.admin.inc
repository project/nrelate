<?php

function nrelate_admin_feed_settings() {
  $form = array();
  
  drupal_add_css(drupal_get_path('module', 'nrelate') . '/nrelate.admin.css');
  
  drupal_add_js(array(
    'nrelate' => array(
      'admin_version' => NRELATE_RELATED_ADMIN_VERSION,
      'domain' => urlencode(str_replace(array('http://','https://'), '', url('<front>', array('absolute' => TRUE)))),
      'key' => variable_get('nrelate_feed_key', ''),
    )
  ), 'setting');
  drupal_add_js(drupal_get_path('module', 'nrelate') . '/js/nrelate.admin.js');
  
  $form['feed'] = array(
    '#type' => 'fieldset',
    '#title' => t('Feed'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );
  $form['feed']['nrelate_feed_key'] = array(
    '#type' => 'item',
    '#title' => t('Feed key'),
    '#description' => t('This is the private key needed to access the custom nrelate content <a href="!link">feed</a> used for indexing.', array('!link' => url('nrelate-rss.xml', array('query' => array('key' => variable_get('nrelate_feed_key', '')), 'absolute' => TRUE)))),
    '#markup' => variable_get('nrelate_feed_key', ''),
  );
  $types = node_type_get_names();
  $form['feed']['nrelate_feed_types'] = array(
    '#type' => 'checkboxes', 
    '#title' => t('Content types allowed in nrelate related content'),
    '#default_value' => variable_get('nrelate_feed_types', array_keys($types)),
    '#options' => $types, 
    //'#description' => t('Users with the %outline-perm permission can add all content types.', array('%outline-perm' => t('Administer book outlines'))), 
    //'#required' => TRUE,
  );
  $fields = field_info_fields();
  $options = array(
    '' => t(' - None - '),
  );
  foreach ($fields as $field) {
    if ($field['type'] == 'image') {
      $options[$field['field_name']] = $field['field_name'];
    }
  }
  $form['feed']['thumbnail']['nrelate_feed_thumbnail_field'] = array(
    '#type' => 'select',
    '#title' => t('Thumbnail field'),
    '#description' => t('If you use a custom image field to represent content, nrelate can use it.'),
    '#options' => $options,
    '#default_value' => variable_get('nrelate_feed_thumbnail_field', ''),
  );
    
  return system_settings_form($form);
}

function nrelate_admin_general_settings() {
  $form = array();
  
  drupal_add_css(drupal_get_path('module', 'nrelate') . '/nrelate.admin.css');
  
  drupal_add_js(array(
    'nrelate' => array(
      'admin_version' => NRELATE_RELATED_ADMIN_VERSION,
      'domain' => urlencode(str_replace(array('http://','https://'), '', url('<front>', array('absolute' => TRUE)))),
      'key' => variable_get('nrelate_feed_key', ''),
    )
  ), 'setting');
  drupal_add_js(drupal_get_path('module', 'nrelate') . '/js/nrelate.admin.js');
  
  $form['ads'] = array(
    '#type' => 'fieldset',
    '#title' => t('Advertising'),
    '#description' => t('Become a part of the nrelate advertising network and earn some extra money on your blog.'),
    '#collapsible' => FALSE,
    '#collapsed' => FALSE,
  );
  $form['ads']['nrelate_account_ad_id'] = array(
    '#type' => 'item',
    '#title' => t('Your ad ID'),
    '#description' => t('This is your ad ID for this site. <a href="!link">Sign up to earn money.</a>', array('!link' => url('https://partners.nrelate.com/register/'))),
    '#markup' => '<div id="getnrcode"></div>',
  );
  
  $form['account'] = array(
    '#type' => 'fieldset',
    '#title' => t('Account'),
    '#collapsible' => FALSE,
    '#collapsed' => FALSE,
  );
  $form['account']['nrelate_account_send_email'] = array(
    '#type' => 'checkbox',
    '#title' => t('Send email address to nrelate'),
    '#description' => t('nrelate may need to communicate with you when we release new features or have a problem accessing your website.</br>  Check the box to send nrelate the site email address (under "<a href="!link">Site Information</a>").  We promise not to overwhelm you with email.', array('!link' => url('admin/config/system/site-information'))),
    '#default_value' => variable_get('nrelate_account_send_email', NRELATE_ACCOUNT_DEFAULT_SEND_EMAIL),
  );
  
  $form['#submit'][] = 'nrelate_admin_general_settings_submit';
  return system_settings_form($form);
}

function nrelate_admin_general_settings_submit($form, &$form_state) {
  $config = array();
  
  // Exclude unnecessary elements.
  form_state_values_clean($form_state);
  
  foreach ($form_state['values'] as $key => $value) {
    if (is_array($value) && isset($form_state['values']['array_filter'])) {
      $value = array_keys(array_filter($value));
    }
    $config[$key] = $value;
  }
  
  // Send config to nrelate
  module_load_include('inc', 'nrelate', 'nrelate.comm');
  nrelate_comm_send_general_settings($config);
}

function nrelate_admin_related_settings() {
  $form = array();
  $form['#attributes'] = array(
    'enctype' => 'multipart/form-data',
  );
  
  drupal_add_css(drupal_get_path('module', 'nrelate') . '/nrelate.admin.css');
  
  drupal_add_js(array(
    'nrelate' => array(
      'admin_version' => NRELATE_RELATED_ADMIN_VERSION,
      'domain' => urlencode(str_replace(array('http://','https://'), '', url('<front>', array('absolute' => TRUE)))),
      'key' => variable_get('nrelate_feed_key', ''),
    )
  ), 'setting');
  drupal_add_js(drupal_get_path('module', 'nrelate') . '/js/nrelate.admin.js');
  
  $form['widget'] = array(
    '#type' => 'fieldset',
    '#title' => t('Widget'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );
  $form['widget']['nrelate_related_widget_label'] = array(
    '#type' => 'textfield',
    '#title' => t('Label'),
    '#description' => t('Enter a label for the related content widget.'),
    '#default_value' => variable_get('nrelate_related_widget_label', NRELATE_RELATED_WIDGET_DEFAULT_LABEL),
  );
  $form['widget']['nrelate_related_widget_num_results'] = array(
    '#type' => 'select',
    '#title' => t('Number of results'),
    '#description' => t('<b>Maximum</b> number of related posts to display from this site.</br><em>To display multiple rows of thumbnails, choose more than will fit in one row.</em>'),
    '#options' => _nrelate_admin_numerical_options(20, TRUE),
    '#default_value' => variable_get('nrelate_related_widget_num_results', NRELATE_RELATED_WIDGET_DEFAULT_NUM_RESULTS),
  );
  $options = array(
    'low' => t('Low: least relevant'),
    'medium' => t('Med: more relevant'),
    'high' => t('High: most relevant'),
  );
  $form['widget']['nrelate_related_widget_relevancy'] = array(
    '#type' => 'select',
    '#title' => t('Relavency'),
    '#description' => t('How relevant do you want the results to be?<br/><i>Based on the amount/type of content on your website, higher relevancy settings may return little or no posts.</i>'),
    '#options' => $options,
    '#default_value' => variable_get('nrelate_related_widget_relevancy', NRELATE_RELATED_WIDGET_DEFAULT_RELAVENCY),
  );
  $form['widget']['maxage'] = array(
    '#type' => 'fieldset',
    '#title' => t('Max age'),
    '#description' => t('How deep into your archive would you like to go for related posts?'),
    '#collapsible' => FALSE,
    '#collapsed' => FALSE,
  );
  $form['widget']['maxage']['nrelate_related_widget_max_age_num'] = array(
    '#type' => 'select',
    '#options' => _nrelate_admin_numerical_options(20, TRUE),
    '#default_value' => variable_get('nrelate_related_widget_max_age_num', NRELATE_RELATED_WIDGET_DEFAULT_MAX_AGE_NUM),
  );
  $options = array(
    'hours'   => t('Hour(s)'),
    'days'    => t('Days(s)'),
    'weeks'   => t('Weeks(s)'),
    'months'  => t('Months(s)'),
    'years'   => t('Years(s)'),
  );
  $form['widget']['maxage']['nrelate_related_widget_max_age_unit'] = array(
    '#type' => 'select',
    '#options' => $options,
    '#default_value' => variable_get('nrelate_related_widget_max_age_unit', NRELATE_RELATED_WIDGET_DEFAULT_MAX_AGE_UNIT),
  );
  $form['widget']['nrelate_related_widget_show_title'] = array(
    '#type' => 'checkbox',
    '#title' => t('Show content title?'),
    '#default_value' => variable_get('nrelate_related_widget_show_title', NRELATE_RELATED_WIDGET_DEFAULT_SHOW_TITLE),
  );
  $form['widget']['nrelate_related_widget_title_size'] = array(
    '#type' => 'select',
    '#title' => t('Content title size'),
    '#description' => t('Maximum number of characters for title?'),
    '#options' => _nrelate_admin_numerical_options(150, FALSE),
    '#default_value' => variable_get('nrelate_related_widget_title_size', NRELATE_RELATED_WIDGET_DEFAULT_TITLE_SIZE),
    '#states' => array(
      'visible' => array(
        ':input[name="nrelate_related_widget_title_size"]' => array('checked' => TRUE),
      ),
    ),
  );
  $form['widget']['nrelate_related_widget_show_excerpt'] = array(
    '#type' => 'checkbox',
    '#title' => t('Show content excerpt?'),
    '#default_value' => variable_get('nrelate_related_widget_show_excerpt', NRELATE_RELATED_WIDGET_DEFAULT_SHOW_EXCERPT),
  );
  $form['widget']['nrelate_related_widget_excerpt_size'] = array(
    '#type' => 'select',
    '#title' => t('Content excerpt size'),
    '#description' => t('Maximum number of words for excerpt?'),
    '#options' => _nrelate_admin_numerical_options(100, FALSE),
    '#default_value' => variable_get('nrelate_related_widget_excerpt_size', NRELATE_RELATED_WIDGET_DEFAULT_EXCERPT_SIZE),
    '#states' => array(
      'visible' => array(
        ':input[name="nrelate_related_widget_show_excerpt"]' => array('checked' => TRUE),
      ),
    ),
  );
  $form['widget']['nrelate_related_widget_mode'] = array(
    '#type' => 'select',
    '#title' => t('Display mode'),
    '#options' => array(
      NRELATE_RELATED_WIDGET_MODE_TEXT        => t('TEXT only'),
      NRELATE_RELATED_WIDGET_MODE_THUMBNAILS  => t('THUMBNAILS with TEXT'),
    ),
    '#default_value' => variable_get('nrelate_related_widget_mode', NRELATE_RELATED_WIDGET_DEFAULT_MODE),
  );
  
  $form['widget']['thumbnail'] = array(
    '#type' => 'fieldset',
    '#title' => t('Thumbnail Style'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
    '#states' => array(
      // Hide the sizes if thumbnails are disabled
      'visible' => array(
        ':input[name="nrelate_related_widget_mode"]' => array('value' => (string) NRELATE_RELATED_WIDGET_MODE_THUMBNAILS),
      ),
    ),
  );
  $form['widget']['thumbnail']['nrelate_related_widget_thumbnail_default'] = array(
    '#type' => 'file',
    '#title' => t('Default thumbnail'),
    '#description' => t('If you do not provide a default image, nrelate will fallback on <a href="!url" target="_blank">these images</a>.', array('!url' => 'http://img.nrelate.com/rcw_dr/' . NRELATE_RELATED_PLUGIN_VERSION . '/defaultImages.html')),
    '#default_value' => variable_get('nrelate_related_widget_thumbnail_default', ''),
  );
  $sizes = array(80,90,100,110,120,130,140,150);
  $options = array();
  foreach ($sizes as $size) {
    $options[$size] = t(
      '<br /> <span class="name">@thumbnail_size</span> <br /> !thumbnail_preview',
      array(
        '@thumbnail_size' => $size,
        '!thumbnail_preview' => theme('image', array(
          'path' => drupal_get_path('module', 'nrelate') . '/images/thumbnails/preview_cloud_' . $size . '.jpeg',
          'attributes' => array('class' => array('thumbnail', 'thumbnail-' . $size)),
        ))
      )
    );
  }
  $form['widget']['thumbnail']['nrelate_related_widget_thumbnail_size'] = array(
    '#type' => 'radios',
    '#title' => t('Thumbnail size'),
    '#options' => $options,
    '#default_value' => variable_get('nrelate_related_widget_thumbnail_size', NRELATE_RELATED_WIDGET_DEFAULT_THUMBNAIL_SIZE),
  );
  $form['widget']['thumbnail']['nrelate_related_widget_thumbnail_style'] = array(
    '#type' => 'radios',
    '#title' => t('Thumbnail style'),
    '#options' => _nrelate_admin_related_widget_style_options(NRELATE_RELATED_WIDGET_MODE_THUMBNAILS),
    '#default_value' => variable_get('nrelate_related_widget_thumbnail_style', NRELATE_RELATED_WIDGET_DEFAULT_THUMBNAIL_STYLE),
  );
  
  $form['widget']['text'] = array(
    '#type' => 'fieldset',
    '#title' => t('Text Style'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
    '#states' => array(
      // Hide the sizes if thumbnails are disabled
      'visible' => array(
        ':input[name="nrelate_related_widget_mode"]' => array('value' => (string) NRELATE_RELATED_WIDGET_MODE_TEXT),
      ),
    ),
  );
  $form['widget']['text']['nrelate_related_widget_text_style'] = array(
    '#type' => 'radios',
    '#title' => t('Text style'),
    '#options' => _nrelate_admin_related_widget_style_options(NRELATE_RELATED_WIDGET_MODE_TEXT),
    '#default_value' => variable_get('nrelate_related_widget_text_style', NRELATE_RELATED_WIDGET_DEFAULT_TEXT_STYLE),
  );
  
  $form['ads'] = array(
    '#type' => 'fieldset',
    '#title' => t('Advertising'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );
  $form['ads']['nrelate_related_widget_ads_show'] = array(
    '#type' => 'checkbox',
    '#title' => t('Would you like to display ads?'),
    '#default_value' => variable_get('nrelate_related_widget_ads_show', NRELATE_RELATED_WIDGET_DEFAULT_ADS_SHOW),
  );
  $form['ads']['nrelate_related_widget_ads_num'] = array(
    '#type' => 'select',
    '#title' => t('Number of ads'),
    '#description' => t('How many ad spaces do you wish to show?'),
    '#options' => _nrelate_admin_numerical_options(10, FALSE),
    '#default_value' => variable_get('nrelate_related_widget_ads_num', NRELATE_RELATED_WIDGET_DEFAULT_ADS_NUM),
    '#states' => array(
      'visible' => array(
        ':input[name="nrelate_related_widget_ads_show"]' => array('checked' => TRUE),
      ),
    ),
  );
  $options = array(
    'mixed' => t('Mixed'),
    'first' => t('First'),
    'last'  => t('Last'),
  );
  $form['ads']['nrelate_related_widget_ads_placement'] = array(
    '#type' => 'select',
    '#title' => t('Ad placement'),
    '#description' => t('Where would you like to place the ads?'),
    '#options' => $options,
    '#default_value' => variable_get('nrelate_related_widget_ads_placement', NRELATE_RELATED_WIDGET_DEFAULT_ADS_PLACEMENT),
    '#states' => array(
      'visible' => array(
        ':input[name="nrelate_related_widget_ads_show"]' => array('checked' => TRUE),
      ),
    ),
  );
  $form['ads']['nrelate_related_widget_ads_animated'] = array(
    '#type' => 'checkbox',
    '#title' => t('Would you like to show animated "sponsored" text in ads?'),
    '#default_value' => variable_get('nrelate_related_widget_ads_animated', NRELATE_RELATED_WIDGET_DEFAULT_ADS_ANIMATED),
    '#states' => array(
      'visible' => array(
        ':input[name="nrelate_related_widget_ads_show"]' => array('checked' => TRUE),
      ),
    ),
  );
  
  $form['labs'] = array(
    '#type' => 'fieldset',
    '#title' => t('nrelate Labs'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );
  $options = array(
    NRELATE_RELATED_WIDGET_LABS_MODE_JS    => t('Javascript: Stable and fast'),
    NRELATE_RELATED_WIDGET_LABS_MODE_NOJS  => t('No Javascript: BETA VERSION - Allows search engines to index our plugin and may help your SEO.'),
  );
  $form['labs']['nrelate_related_widget_labs_mode'] = array(
    '#type' => 'select',
    '#title' => t('Version'),
    '#description' => t('Which nrelate version would you like to use?'),
    '#options' => $options,
    '#default_value' => variable_get('nrelate_related_widget_labs_mode', NRELATE_RELATED_WIDGET_DEFAULT_LABS_MODE),
  );
  
  $form['#submit'][] = 'nrelate_admin_related_settings_submit';
  return system_settings_form($form);
}

function nrelate_admin_related_settings_submit($form, &$form_state) {
  $config = array();
  
  // Exclude unnecessary elements.
  form_state_values_clean($form_state);
  
  foreach ($form_state['values'] as $key => $value) {
    if (is_array($value) && isset($form_state['values']['array_filter'])) {
      $value = array_keys(array_filter($value));
    }
    $config[$key] = $value;
  }
  
  // Send config to nrelate
  module_load_include('inc', 'nrelate', 'nrelate.comm');
  nrelate_comm_send_related_settings($config);
}

function _nrelate_admin_numerical_options($max, $include_zero = FALSE) {
  $options = array();
  
  if ($include_zero) {
    $options[0] = 0;
  }
  
  for ($i=1; $i<=$max; $i++) {
    $options[$i] = $i;
  }
  
  return $options;
}

function _nrelate_admin_related_widget_styles() {
  return array(
    /** Thumbnail & Text format **/
    NRELATE_RELATED_WIDGET_MODE_THUMBNAILS => array(
      'default' => array(
        'stylesheet' => 'nrelate-panels-default.css',
        'name' => t('Default'),
        'features' => t('<ul>
                          <li>Hover effects</li>
                          <li>Border</li>
                          <li>Left aligned text</li>
                        </ul>'),
        'info' => t('We developed this style as a simple way to place nrelate content on any site. We wanted to keep everything simple, so that the focus is on your site\'s content.')
      ),
      'bty' => array(
        'stylesheet' => 'nrelate-panels-bty.css',
        'name' => t('Bloginity.com'),
        'features' => t('<ul>
                          <li>No Border</li>
                          <li>Centered text</li>
                          <li>No Hover</li>
                        </ul>'),
        'info' => t('This style is based upon the custom version developed at <a href="http://bloginity.com" target="_blank">Bloginity.com</a>.<br>Bloginity is a a young, vibrant online magazine, fashion blog, and source of culture entertainment news.')
      ),
      'dhot' => array(
        'stylesheet' => 'nrelate-panels-dhot.css',
        'name' => t('LinkWithin'),
        'features' => t('<ul>
                          <li>All Capital text</li>
                          <li>Hover Effects</li>
                          <li>Stylized thumbnail dividing line</li>
                        </ul>'),
        'info' => t('This style is based upon the related thumbnail widget from <a href="http://linkwithin.com" target="_blank">Linkwithin.com</a>. LinkWithin is a blog widget that appears under each post')
      ),
      'huf' => array(
        'stylesheet' => 'nrelate-panels-huf.css',
        'name' => t('Huffingtonpost'),
        'features' => t('<ul>
                          <li>Side Layout</li>
                          <li>No Hover Effect</li>
                          <li>Built for Showing Excerpt</li>
                        </ul>'),
        'info' => t('This style is based upon the related content area on <a href="http://huffingtonpost.com" target="_blank">huffingtonpost.com</a>. The Huffington Post is an American news website and aggregated blog.')
      ),
      'tre' => array(
        'stylesheet' => 'nrelate-panels-tre.css',
        'name' => t('Trendland.net'),
        'features' => t('<ul>
                          <li>Hover effects</li>
                          <li>Border</li>
                          <li>Left aligned text</li>
                          <li>Text over thumbnail</li>
                          <li>Semi-transparent text background</li>
                        </ul>'),
        'info' => t('This style is based upon the custom version developed at <a href="http://trendland.net" target="_blank">Trendland.net</a>.<br>Trendland is an online magazine that redefines trend forecasting through a rich visual journey.')
      ),
      'pol' => array(
        'stylesheet' => 'nrelate-panels-pol.css',
        'name' => t('Polaroid'),
        'features' => t('<ul>
                          <li>Turns your images into Polaroid pictures.</li>
                          <li>Rotated randomly.</li>
                        </ul>'),
        'info' => t('Polaroid style images. Inspired by <a href="http://www.zurb.com/playground/css3-polaroids" target="_blank">this article</a>.<br>at Zurb.com.')
      ),
      'none' => array(
        'name' => t('none'),
        'features' => t('<ul>
                          <li>Allows you to create your own css</li>
                        </ul>'),
        'info' => t('Selecting this option will disable all nrelate styles, allowing you to create your own.')
      ),
    ),
    
    /** Text only format **/
    NRELATE_RELATED_WIDGET_MODE_TEXT => array(
      'default' => array(
        'stylesheet' => 'nrelate-panels-text.css',
        'name' => t('Default'),
        'features' => t('<ul>
        <li>Simple</li>
        <li>Left aligned text</li>
        </ul>'),
        'info' => t('We developed this style as a simple way to place nrelate content on any site. We wanted to keep everything simple, so that the focus is on your site\'s content.')
      ),
      'none' => array(
        'name' => t('none'),
        'features' => t('<ul>
        <li>Allows you to create your own css</li>
        </ul>'),
        'info' => t('Selecting this option will disable all nrelate styles, allowing you to create your own.')
      ),
    )
  );
}

function _nrelate_admin_related_widget_style_options($type) {
  $options = array();
  $styles = _nrelate_admin_related_widget_styles();
  
  foreach ($styles[$type] as $style_id => $style) {
    $img = theme('image', array(
      'path' => drupal_get_path('module', 'nrelate') . '/images/thumbnail_style_' . $style_id . '.png',
      'attributes' => array('class' => array('style', 'style-' . $style_id)),
    ));
    $info = $style['info'];
    if ($style_id != 'none') {
      $info .= '<div><a href="' . NRELATE_CSS_URL . 'nrelate-panels-' . $style_id .'.css" title="CSS for ' . $style['name'] . ' Style">View Stylesheet</a></div>';
    }
    $options[$style_id] = <<<EOD
      <div class="name">{$style['name']}</div>
      <div class="img">{$img}</div>
      <div class="features">{$style['features']}</div>
      <div class="info">{$info}</div>
EOD;
  }
  
  return $options;
}
