<?php

/**
 * Report activation back to nrelate servers
 *
 * TODO: This is modeled after the WP plugin and needs testing/work.
 */
function nrelate_comm_send_activation($activate = TRUE) {
  // Send notification to nrelate server of activation
  $action = $activate ? 'ACTIVATE' : 'DEACTIVATE';
  $data = array(
    'DOMAIN'        => _nrelate_get_domain(),
    'ACTION'        => $action,
    'RSSMODE'       => 'FULL', // 'SUMMARY  // TODO: why are we passing this?
    'VERSION'       => NRELATE_RELATED_PLUGIN_VERSION,
    'KEY'           => variable_get('nrelate_feed_key', ''),
    'ADMINVERSION'  => NRELATE_RELATED_ADMIN_VERSION,
    'PLUGIN'        => 'related',
    'RSSURL'        => url('nrelate-rss.xml', array('absolute' => TRUE)),
  );
  $url = 'http://api.nrelate.com/common_dr/' . NRELATE_RELATED_ADMIN_VERSION . '/drupalnotify_activation.php';
  
  $options['method'] = 'POST';
  $options['data'] = http_build_query($data);
  $options['headers'] = array('Content-Type' => 'application/x-www-form-urlencoded');
  $result = drupal_http_request($url, $options);
  
  if ($result->code != 200) {
    drupal_set_message('Error: ' . check_plain($result->error));
  }
}

/**
 * Report general settings back to nrelate servers
 *
 * TODO: This is modeled after the WP plugin and needs testing/work.
 */
function nrelate_comm_send_general_settings($config) {
  $url = 'http://api.nrelate.com/rcw_dr/'. NRELATE_RELATED_ADMIN_VERSION .'/processDrupalAdmin.php';
  $options = array();
  
  $data = array(
    'DOMAIN'    => _nrelate_get_domain(),
    'EMAIL'     => $config['nrelate_account_send_email'] ? variable_get('site_mail', '') : '',
    'EMAILOPT'  => $config['nrelate_account_send_email'], // Must use raw config since variable might not have been saved yet.
    'RSSURL'    => url('nrelate-rss.xml', array('absolute' => TRUE)),
    'KEY'       => variable_get('nrelate_feed_key', ''),
    'CONFIG'    => drupal_json_encode($config),
  );
  
  $options['method'] = 'POST';
  $options['data'] = http_build_query($data);
  $options['headers'] = array('Content-Type' => 'application/x-www-form-urlencoded');
  $result = drupal_http_request($url, $options);
  
  if ($result->code != 200) {
    drupal_set_message('Error contacting nrelate servers: ' . check_plain($result->error));
  }
}

/**
 * Report related-widget settings back to nrelate servers
 *
 * TODO: This is modeled after the WP plugin and needs testing/work.
 */
function nrelate_comm_send_related_settings($config) {
  $url = 'http://api.nrelate.com/rcw_dr/'. NRELATE_RELATED_PLUGIN_VERSION .'/processDRrelated.php';
  $options = array();
  
  $data = array(
    'DOMAIN'    => _nrelate_get_domain(),
    'EMAIL'     => variable_get('nrelate_account_send_email', NRELATE_ACCOUNT_DEFAULT_SEND_EMAIL) ? variable_get('site_mail', '') : '',
    'EMAILOPT'  => variable_get('nrelate_account_send_email', NRELATE_ACCOUNT_DEFAULT_SEND_EMAIL),
    'RSSURL'    => url('nrelate-rss.xml', array('absolute' => TRUE)),
    'KEY'       => variable_get('nrelate_feed_key', ''),
    'CONFIG'    => drupal_json_encode($config),
  );
  
  $options['method'] = 'POST';
  $options['data'] = http_build_query($data);
  $options['headers'] = array('Content-Type' => 'application/x-www-form-urlencoded');
  $result = drupal_http_request($url, $options);
  
  if ($result->code != 200) {
    drupal_set_message('Error contacting nrelate servers: ' . check_plain($result->error));
  }
}
